package com.openix.exercise1.error;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorType {

    BAD_REQUEST("Bad request", "E_400"),
    NOT_FOUND("Resource not found", "E_404"),
    CONFLICT_REQUEST("We could not complete your request", "E_409");

    private String msg;
    private String code;

}
