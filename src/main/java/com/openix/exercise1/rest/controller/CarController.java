package com.openix.exercise1.rest.controller;

import com.openix.exercise1.rest.response.CarResponse;
import com.openix.exercise1.service.CarService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("car")
public class CarController {

    @Autowired
    private CarService carService;

    @GetMapping(value = "/")
    public List<CarResponse> findAllBySpecification(
            @Parameter(name ="filter", schema = @Schema(description = "filter dynamic",type = "string", defaultValue = "modelName:Rena*"))
            @RequestParam(value = "filter") String filter
    ) {
        return carService.filterDynamic(filter).stream().map(CarResponse::new).collect(Collectors.toList());
    }

    @GetMapping("/find")
    public CarResponse findCar(
            @Parameter(name ="date", schema = @Schema(description = "Date by find",type = "string", defaultValue = "2021-03-10"))
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
            @Parameter(name ="carId", schema = @Schema(description = "ID",type = "number", defaultValue = "2"))
            @RequestParam Long carId
    ) {
        return new CarResponse(carService.findCard(date, carId));
    }

    @GetMapping("/download/cars.xlsx")
    public void downloadExcel(HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=cars.xlsx");
        IOUtils.copy(carService.generateExcel(), response.getOutputStream());
    }
}
