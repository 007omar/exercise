package com.openix.exercise1.rest.response;

import com.openix.exercise1.data.entity.Cost;
import lombok.Getter;

import java.time.LocalDate;

@Getter
public class CostResponse {
    private final Double price;
    private final LocalDate dateStart;
    private final LocalDate dateEnd;

    public CostResponse(Cost c) {
        this.price = c.getPrice();
        this.dateStart = c.getDateStart();
        this.dateEnd = c.getDateEnd();
    }
}
