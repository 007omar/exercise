package com.openix.exercise1.rest.response;

import com.openix.exercise1.data.entity.Car;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
public class CarResponse {
    private final Long carId;
    private final Long brandId;
    private final List<CostResponse> prices;

    public CarResponse(Car car) {
        this.carId = car.getId();
        this.brandId =  car.getBrand().getId();
        this.prices = car.getCosts().stream().map(CostResponse::new).collect(Collectors.toList());
    }
}
