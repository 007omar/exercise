package com.openix.exercise1.service;

import com.google.common.base.Joiner;
import com.openix.exercise1.data.entity.Car;
import com.openix.exercise1.data.repository.CarRepository;
import com.openix.exercise1.data.specification.CarSpecification;
import com.openix.exercise1.data.specification.builder.CarSpecificationsBuilder;
import com.openix.exercise1.util.SearchOperation;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CarService {

    @Autowired
    private CarRepository carRepository;


    public List<Car> filterDynamic(String filter) {
        CarSpecificationsBuilder builder = new CarSpecificationsBuilder();
        String operationSetExper = Joiner.on("|")
                .join(SearchOperation.SIMPLE_OPERATION_SET);
        Pattern pattern = Pattern.compile("(\\w+?)(" + operationSetExper + ")(\\p{Punct}?)(\\w+?)(\\p{Punct}?),");
        Matcher matcher = pattern.matcher(filter + ",");
        while (matcher.find()) {
            builder.with(matcher.group(1), matcher.group(2), matcher.group(4), matcher.group(3), matcher.group(5));
        }

        Specification<Car> spec = builder.build();
        return  carRepository.findAll(spec);
    }

    public Car findCard(LocalDate date, Long carId) {
        return carRepository.findOne(CarSpecification.findCar(date, carId)).orElseThrow(NoSuchElementException::new);
    }

    public ByteArrayInputStream generateExcel() {
        return carToExcelFile(Arrays.asList("ID", "MODEL NAME", "BRAND", "COLOR", "PRICE"), carRepository.findAll());
    }

    private ByteArrayInputStream carToExcelFile(List<String> headers, List<Car> cars) {
        try (Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("List of cars");

            createHeader(headers, workbook, sheet);
            loadData(cars, sheet);
            autoSize(headers, sheet);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return new ByteArrayInputStream(outputStream.toByteArray());
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private void createHeader(List<String> headers, Workbook workbook, Sheet sheet) {
        Row row = sheet.createRow(0);
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        int index = 0;
        for (String header : headers) {
            Cell cell = row.createCell(index);
            cell.setCellValue(header);
            cell.setCellStyle(headerCellStyle);
            index++;
        }

    }

    private void loadData(List<Car> cars, Sheet sheet) {
        int index = 0;
        for (Car car : cars) {
            Row dataRow = sheet.createRow(index + 1);
            dataRow.createCell(0).setCellValue(car.getId());
            dataRow.createCell(1).setCellValue(car.getModelName());
            dataRow.createCell(2).setCellValue(car.getBrand().getName());
            dataRow.createCell(3).setCellValue(car.getColor().toString());
            dataRow.createCell(4).setCellValue(car.firstCost());
            index++;
        }
    }

    private void autoSize(List<String> headers, Sheet sheet) {
        for (int i = 0; i < headers.size(); i++) {
            sheet.autoSizeColumn(i);
        }
    }
}
