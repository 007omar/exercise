package com.openix.exercise1.data.entity;

import com.openix.exercise1.data.enums.Color;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

@Entity
@Getter
@Setter
public class Car implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Model Name is mandatory")
    @Column(length = 100)
    private String modelName;

    @NotBlank(message = "Color is mandatory")
    @Enumerated(EnumType.STRING)
    @Column(length = 40)
    private Color color;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "brand_id")
    private Brand brand;

    @OneToMany(mappedBy = "car", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<Cost> costs;

    public Double firstCost() {
        return costs.isEmpty() ? 0 : costs.get(0).getPrice();
    }
}
