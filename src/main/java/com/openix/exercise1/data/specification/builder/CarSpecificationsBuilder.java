package com.openix.exercise1.data.specification.builder;

import com.openix.exercise1.data.entity.Car;
import com.openix.exercise1.data.specification.CarFilterSpecification;
import com.openix.exercise1.util.SearchOperation;
import com.openix.exercise1.util.SpecSearchCriteria;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;


public final class CarSpecificationsBuilder {

    private final List<SpecSearchCriteria> params;

    public CarSpecificationsBuilder() {
        params = new ArrayList<>();
    }

    public final CarSpecificationsBuilder with(final String key, final String operation, final Object value, final String prefix, final String suffix) {
        return with(null, key, operation, value, prefix, suffix);
    }

    public final CarSpecificationsBuilder with(final String orPredicate, final String key, final String operation, final Object value, final String prefix, final String suffix) {
        SearchOperation op = SearchOperation.getSimpleOperation(operation.charAt(0));
        if (op != null) {
            if (op == SearchOperation.EQUALITY) {
                final boolean startWithAsterisk = prefix != null && prefix.contains(SearchOperation.ZERO_OR_MORE_REGEX);
                final boolean endWithAsterisk = suffix != null && suffix.contains(SearchOperation.ZERO_OR_MORE_REGEX);

                if (startWithAsterisk && endWithAsterisk) {
                    op = SearchOperation.CONTAINS;
                } else if (startWithAsterisk) {
                    op = SearchOperation.ENDS_WITH;
                } else if (endWithAsterisk) {
                    op = SearchOperation.STARTS_WITH;
                }
            }
            params.add(new SpecSearchCriteria(orPredicate, key, op, value));
        }
        return this;
    }

    public Specification<Car> build() {
        if (params.size() == 0)
            return null;

        Specification<Car> result = new CarFilterSpecification(params.get(0));

        for (int i = 1; i < params.size(); i++) {
            result = params.get(i).isOrPredicate()
                    ? Specification.where(result).or(new CarFilterSpecification(params.get(i)))
                    : Specification.where(result).and(new CarFilterSpecification(params.get(i)));
        }

        return result;
    }

    public final CarSpecificationsBuilder with(CarFilterSpecification spec) {
        params.add(spec.getCriteria());
        return this;
    }

    public final CarSpecificationsBuilder with(SpecSearchCriteria criteria) {
        params.add(criteria);
        return this;
    }
}
