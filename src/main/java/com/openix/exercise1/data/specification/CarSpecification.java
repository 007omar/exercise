package com.openix.exercise1.data.specification;

import com.openix.exercise1.data.entity.Car;
import com.openix.exercise1.data.entity.Cost;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CarSpecification {


    private CarSpecification() {
    }

    public static Specification<Car> findCar(LocalDate date, Long carId) {
        return (root, query, cb) -> {
            final List<Predicate> predicateList = new ArrayList<>();
            Join<Car, Cost> carCostsJoin = root.join("costs");

            predicateList.add(cb.equal(root.get("id"), carId));
            predicateList.add(cb.lessThan(carCostsJoin.get("dateStart"), date));
            predicateList.add(cb.greaterThan(carCostsJoin.get("dateEnd"), date));

            return cb.and(predicateList.toArray(Predicate[]::new));
        };
    }
}
