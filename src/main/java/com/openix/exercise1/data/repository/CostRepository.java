package com.openix.exercise1.data.repository;

import com.openix.exercise1.data.entity.Cost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CostRepository extends JpaRepository<Cost, Long>, JpaSpecificationExecutor<Cost> {
}
