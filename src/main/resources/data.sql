DROP TABLE IF EXISTS cost;
DROP TABLE IF EXISTS car;
DROP TABLE IF EXISTS brand;

CREATE TABLE brand
(
    id   INT          NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE car
(
    id         INT          NOT NULL AUTO_INCREMENT,
    model_name VARCHAR(100) NOT NULL,
    color      VARCHAR(40)  NOT NULL,
    brand_id   INT          NOT NULL,
    FOREIGN KEY (brand_id) REFERENCES brand (id),
    PRIMARY KEY (id)
);

CREATE TABLE cost
(
    id         INT   NOT NULL AUTO_INCREMENT,
    date_start DATE  NOT NULL,
    date_end   DATE  NOT NULL,
    price      FLOAT NOT NULL,
    car_id     INT   NOT NULL,
    FOREIGN KEY (car_id) REFERENCES car (id),
    PRIMARY KEY (id)
);

INSERT INTO brand (id, name)
VALUES (1, 'Alfa Romeo'),
       (2, 'Alpine'),
       (3, 'Audi'),
       (4, 'BMW'),
       (5, 'Renault'),
       (6, 'Nissan'),
       (7, 'Ferrari'),
       (8, 'Fiat'),
       (9, 'Ford'),
       (10, 'Honda'),
       (11, 'Hyundai'),
       (12, 'Jeep'),
       (13, 'KIA'),
       (14, 'Lotus'),
       (15, 'Maserati');

INSERT INTO car (id, model_name, color, brand_id)
VALUES (1, 'Audi A3 Cabrio', 'SCARLET', 3),
       (2, 'Renault Captur', 'BEIGE', 5);

INSERT INTO cost (id, car_id, date_start, date_end, price)
VALUES (1, 1, '2020-12-01', '2021-06-01', 37.986),
       (2, 2, '2020-12-01', '2021-06-01', 26.215);




